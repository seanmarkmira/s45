// Base Imports
import React, { useContext, Fragment } from "react";
import { Link, NavLink, useNavigate } from "react-router-dom";

//useNavigate over useHistory

// Bootstrap components
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";

import UserContext from "../UserContext";

const AppNavBar = () => {
    const { user, unsetUser } = useContext(UserContext);
    const navigateTo = useNavigate();

    const logout = () => {
        unsetUser();
        navigateTo("/login");
    };

    let rightNav =
        user.email === null ? (
            <Fragment>
                <Nav.Link as={NavLink} to="/login">
                    Login
                </Nav.Link>
                <Nav.Link as={NavLink} to="/register">
                    Register
                </Nav.Link>
            </Fragment>
        ) : (
            <Fragment>
                <Nav.Link as={NavLink} to="/logout" onClick={unsetUser}>
                    Logout
                </Nav.Link>
            </Fragment>
        );

    return (
        // <Navbar bg="light" expand="lg">
        //     <Navbar.Brand href="#home">Zuitt Booking</Navbar.Brand>
        //     <Navbar.Toggle aria-controls="basic-navbar-nav" />
        //     <Navbar.Collapse id="basic-navbar-nav">
        //         <Nav className="mr-auto">
        //             <Nav.Link href="#home">Home</Nav.Link>
        //             <Nav.Link href="#link">Courses</Nav.Link>
        //         </Nav>
        //     </Navbar.Collapse>
        // </Navbar>
        <Navbar bg="light" expand="lg">
            <Navbar.Brand as={Link} to="/">
                Zuitt Booking
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    {/* <Nav.Link as={NavLink} to="/login"> */}
                    {/*     Login */}
                    {/* </Nav.Link> */}
                    <Nav.Link as={NavLink} to="/">
                        Home
                    </Nav.Link>
                    <Nav.Link as={NavLink} to="/courses">
                        Courses
                    </Nav.Link>
                    <Nav className="ml-auto">{rightNav}</Nav>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
};

export default AppNavBar;
