const NotFound = () => {
	return (
		<>
			<p>Zuitt Booking</p>
			<h3>Page Not Found</h3>
			<p>
				Go back to the <a href="/">homepage</a>
			</p>
		</>
	);
};

export default NotFound;
