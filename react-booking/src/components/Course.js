// Bootstrap Components
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import { useState, useEffect } from "react";

const Course = (props) => {
    //The props is coming from the pages Courses: within the <Book courses={props}>
    let course = props.course;

    // const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(30);
    const [isDisabled, setIsDisabled] = useState(false);

    useEffect(() => {
        if (seats === 0) {
            setIsDisabled(true);
        }
    }, [seats]);
    // const enroll = () => {
    //     if (seats == 0) {
    //         alert("Course is full");
    //     } else {
    //         setCount(count + 1);
    //         setSeats(seats - 1);
    //     }
    // };

    return (
        <Card>
            <Card.Body>
                <Card.Title>{course.name}</Card.Title>
                <b>Description:</b>
                <Card.Text>{course.description}</Card.Text>
                <b>Price:</b>
                <Card.Text>{course.price}</Card.Text>
                <h6>Seats</h6>
                <p>{seats} remaining</p>
                {/* <p>{count}</p> */}
                <Button
                    variant="primary"
                    onClick={() => {
                        setSeats(seats - 1);
                    }}
                    disabled={isDisabled}
                >
                    Enroll
                </Button>
            </Card.Body>
        </Card>
    );
};

export default Course;
