import { useState, useEffect } from "react";

//Bootstrap
import Container from "react-bootstrap/Container";

const Counter = () => {
	const [count, setCount] = useState(0);

	useEffect(() => {
		document.title = `You click ${count} times`;
		console.log("useEffect");
	}, [count]);

	return (
		<Container fluid>
			<p>You Click {count} times</p>
			<button
				onClick={() => {
					setCount(count + 1);
				}}
			>
				Click me
			</button>
		</Container>
	);
};

export default Counter;
