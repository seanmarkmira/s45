import { useState, useEffect } from "react";
import React, { useContext, Fragment } from "react";
//Bootstrap components
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";

import UserContext from "../UserContext";
import { Link, NavLink, useNavigate } from "react-router-dom";
import Nav from "react-bootstrap/Nav";

const Register = () => {
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [passwordConfirm, setPasswordConfirm] = useState("");
	const [isDisabled, setIsDisabled] = useState(true);

	//This is the get the user and unsetUser vars from the UserContext
	const { user, unsetUser } = useContext(UserContext);

	//This is for the useNavigate
	const navigateTo = useNavigate();

	useEffect(() => {
		if (user.email) {
			navigateTo("/");
		}
	}, []);

	// }, []);
	// 	useEffect(() => {
	// 		console.log(email);
	// 	}, [email]);
	//
	// 	useEffect(() => {
	// 		console.log(password);
	// 	}, [password]);
	//
	// 	useEffect(() => {
	// 		console.log(passwordConfirm);
	// 	}, [passwordConfirm]);

	useEffect(() => {
		let isEmailisNotEmpty = email !== "";
		let isPasswordNotEmpty = password !== "";
		let isPasswordConfirmNotEmpty = passwordConfirm !== "";
		let isPasswordMatched = passwordConfirm === password;

		if (
			isEmailisNotEmpty &&
			isPasswordNotEmpty &&
			isPasswordConfirmNotEmpty &&
			isPasswordMatched
		) {
			console.log("Changed to clickable");
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [email, password, passwordConfirm]);

	const register = (e) => {
		console.log("form submitted");
		e.preventDefault();
		alert("Registration Successfully!");
	};
	return (
		<Container fluid>
			<h3>Register</h3>
			<Form onSubmit={register}>
				<Form.Group>
					<Form.Label>Email address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email"
						required
						onChange={(e) => setEmail(e.target.value)}
					/>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Password"
						required
						onChange={(e) => setPassword(e.target.value)}
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Verify Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Verify Password"
						required
						onChange={(e) => setPasswordConfirm(e.target.value)}
					/>
				</Form.Group>
				<Button variant="primary" type="submit" disabled={isDisabled}>
					Submit
				</Button>
			</Form>
		</Container>
	);
};

export default Register;
