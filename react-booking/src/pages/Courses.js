import { Fragement, useEffect, useState } from "react";

//import the component
import Course from "../components/Course";

//import Container from bootstrap
import Container from "react-bootstrap/Container";

//data imports
// import courses from "../mock-data/courses";

const Courses = () => {
	const [courses, setCourses] = useState();
	// Retrieves the courses data from DB
	useEffect(() => {
		fetch("http://localhost:4000/api/courses")
			.then((res) => res.json())
			.then((data) => {
				setCourses(
					data.map((course) => {
						return (
							<CourseCard key={course._id} courseProp={course} />
						);
					})
				);
			});
	}, []);

	return <Container fluid>{CourseCards}</Container>;
};

export default Courses;
