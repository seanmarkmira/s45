import { useState, useEffect, useContext } from "react";
import { Navigate } from "react-router-dom";
//Bootstrap components
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";

//Utilizing the useContext from UserContext
import UserContext from "../UserContext";

const Login = () => {
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isDisabled, setIsDisabled] = useState(true);
	const { user, setUser } = useContext(UserContext);

	console.log(user, setUser);

	useEffect(() => {
		let isEmailisNotEmpty = email !== "";
		let isPasswordNotEmpty = password !== "";

		if (isEmailisNotEmpty && isPasswordNotEmpty) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [email, password]);

	const login = (e) => {
		e.preventDefault();
		alert("Login Successfully!");

		localStorage.setItem("email", email);

		setUser({ email: email });
		console.log(email);
		setEmail("");
		setPassword("");
	};

	if (user.email !== null) {
		return <Navigate to="/" />;
	}

	return (
		<Container fluid>
			<h3>Login</h3>
			<Form onSubmit={login}>
				<Form.Group>
					<Form.Label>Email address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter Email"
						required
						onChange={(e) => setEmail(e.target.value)}
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Password"
						required
						onChange={(e) => setPassword(e.target.value)}
					/>
				</Form.Group>
				<Button variant="primary" type="submit" disabled={isDisabled}>
					Login
				</Button>
			</Form>
		</Container>
	);
};

export default Login;
