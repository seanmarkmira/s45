import React from "react";

//Importing Bootstrap itself
import "bootstrap/dist/css/bootstrap.min.css";
//we are extracting bootstrap components
import Container from "react-bootstrap/Container";

//importing our CSS file
import "./index.css";

// Base Imports
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Routes } from "react-router-dom";

//Components
import Counter from "./components/Counter";
import AppNavBar from "./components/AppNavBar";
import Banner from "./components/Banner";
import Highlights from "./components/Highlights";
import NotFound from "./components/NotFound";

//Page Components
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Courses from "./pages/Courses";

import { useState, useEffect } from "react";

import UserContext from "./UserContext";

// import { createContext } from "react";
//export const NameContext = createContext();

const App = () => {
	// const [test, setTest] = useState(true);
	const [user, setUser] = useState({ email: localStorage.getItem("email") });

	const unsetUser = () => {
		localStorage.clear();
		setUser({ email: null });
	};

	return (
		<UserContext.Provider value={{ user, setUser, unsetUser }}>
			<BrowserRouter>
				<AppNavBar unsetUser />
				{/* {test && <NotFound />} */}
				<Routes>
					<Route path="/" element={<Home />} />
					<Route path="/courses" element={<Courses />} />
					<Route path="/register" element={<Register />} />
					<Route path="/login" element={<Login />} />
					<Route path="*" element={<NotFound />} />
				</Routes>
			</BrowserRouter>
		</UserContext.Provider>
	);
};

export default App;
