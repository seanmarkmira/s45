import React from "react";

//Importing Bootstrap itself
import "bootstrap/dist/css/bootstrap.min.css";
//we are extracting bootstrap components
import Container from "react-bootstrap/Container";

//importing our CSS file
import "./index.css";

// Base Imports
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Routes } from "react-router-dom";

//Components
import Counter from "./components/Counter";
import AppNavBar from "./components/AppNavBar";
import Banner from "./components/Banner";
import Highlights from "./components/Highlights";

//Page Components
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Courses from "./pages/Courses";
import App from "./App";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
//reportWebVitals();
